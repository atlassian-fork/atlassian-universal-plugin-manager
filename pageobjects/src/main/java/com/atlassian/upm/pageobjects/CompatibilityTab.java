package com.atlassian.upm.pageobjects;


import javax.inject.Inject;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.atlassian.webdriver.utils.Check;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import static com.atlassian.upm.pageobjects.Waits.anyAreVisible;
import static com.atlassian.upm.pageobjects.Waits.selectedAndLoaded;
import static com.google.common.collect.Collections2.filter;
import static com.google.common.collect.Collections2.transform;

public class CompatibilityTab extends UPMTab
{
    private static final String COMPATIBILITY_CONTENT_SECTIONS_SELECTOR = "#upm-compatibility-content div.upm-compatibility-category";
    private static final By COMPATIBILITY_CONTENT_SECTIONS = By.cssSelector(COMPATIBILITY_CONTENT_SECTIONS_SELECTOR);
    private static final String INCOMPATIBLE_PLUGINS_ID = "upm-incompatible-plugins";
    private static final By INCOMPATIBLE_PLUGINS_SECTION = By.id(INCOMPATIBLE_PLUGINS_ID);
    private static final By INCOMPATIBLE_PLUGINS = By.cssSelector("#" + INCOMPATIBLE_PLUGINS_ID + " div.upm-plugin");
    private static final String NEED_UPGRADE_PLUGINS_ID = "upm-need-upgrade-plugins";
    private static final By NEED_UPGRADE_PLUGINS = By.id(NEED_UPGRADE_PLUGINS_ID);
    private static final String NEED_PRODUCT_UPGRADE_PLUGINS_ID = "upm-need-product-upgrade-plugins";
    private static final By NEED_PRODUCT_UPGRADE_PLUGINS_SECTION = By.id(NEED_PRODUCT_UPGRADE_PLUGINS_ID);
    private static final By NEED_PRODUCT_UPGRADE_PLUGINS = By.cssSelector("#" + NEED_PRODUCT_UPGRADE_PLUGINS_ID + " div.upm-plugin");
    private static final String COMPATIBLE_PLUGINS_ID = "upm-compatible-plugins";
    private static final By COMPATIBLE_PLUGINS_SECTION = By.id(COMPATIBLE_PLUGINS_ID);
    private static final By COMPATIBLE_PLUGINS = By.cssSelector("#" + COMPATIBLE_PLUGINS_ID + " div.upm-plugin");
    private static final String UNKNOWN_PLUGINS_ID = "upm-unknown-plugins";
    private static final By UNKNOWN_PLUGINS_SECTION = By.id(UNKNOWN_PLUGINS_ID);
    private static final By UNKNOWN_PLUGINS = By.cssSelector("#" + UNKNOWN_PLUGINS_ID + " div.upm-plugin");
    private static final String ALL_PLUGINS_SELECTOR = COMPATIBILITY_CONTENT_SECTIONS_SELECTOR + " div.upm-plugin";
    private static final By ALL_PLUGINS = By.cssSelector(ALL_PLUGINS_SELECTOR);
    private static final String RECENT_PRODUCT_WARNING_ID = "upm-recent-product-release-container";
    private static final By RECENT_PRODUCT_WARNING = By.id(RECENT_PRODUCT_WARNING_ID);
    
    private static final By HIDDEN_INPUT = By.cssSelector("input.upm-plugin-link-self");
    private static final By PLUGIN_ROW = By.cssSelector(".upm-plugin-row");
    private static final By DISABLE_BUTTON = By.cssSelector(".upm-plugin-actions .upm-disable");
    private static final By UPGRADE_BUTTON = By.cssSelector(".upm-plugin-actions .upm-upgrade");
    
    private @Inject AtlassianWebDriver driver;

    @FindBy(id="upm-panel-compatibility")
    private WebElement compatibilityPanel;
    
    @FindBy(id="upm-compatibility-version")
    private WebElement versionPicker;
    
    @FindBy(css="#upm-compatibility-form input.submit")
    private WebElement checkButton;
    
    @WaitUntil
    public void waitUntilTabIsLoaded()
    {
        driver.waitUntil(selectedAndLoaded(compatibilityPanel));
    }
    
    public void selectVersion(Version version)
    {
        new Select(versionPicker).selectByValue(version.value);
        checkButton.click();
        driver.waitUntil(anyAreVisible(COMPATIBILITY_CONTENT_SECTIONS));
    }

    @Override
    protected String getPluginContainerId(TabCategory category)
    {
        return "#upm-" + category.getOptionValue() + "-plugins";
    }

    public Iterable<String> getVisiblePlugins()
    {
        return getVisiblePlugins(ALL_PLUGINS);
    }

    public Iterable<String> getVisibleCompatiblePlugins()
    {
        return getVisiblePlugins(COMPATIBLE_PLUGINS);
    }

    public Iterable<String> getVisibleIncompatiblePlugins()
    {
        return getVisiblePlugins(INCOMPATIBLE_PLUGINS);
    }

    public Iterable<String> getVisibleProductUpgradePlugins()
    {
        return getVisiblePlugins(NEED_PRODUCT_UPGRADE_PLUGINS);
    }

    public Iterable<String> getVisibleUnknownPlugins()
    {
        return getVisiblePlugins(UNKNOWN_PLUGINS);
    }

    public boolean isRecentProductWarningDisplayed()
    {
        return driver.elementIsVisible(RECENT_PRODUCT_WARNING);
    }
    
    private Iterable<String> getVisiblePlugins(By by)
    {
        return transform(filter(driver.findElements(by), new Predicate<WebElement>()
        {
            public boolean apply(WebElement input)
            {
                return input.isDisplayed();
            }
        }), new Function<WebElement, String>()
        {
            public String apply(WebElement from)
            {
                return from.getText();
            }
        });

    }

    /**
     * Find the heading for the specified plugin, and click on it
     * 
     * @param pluginKey the key of the plugin to expand
     * @return the container div for the plugin specified
     */
    public WebElement expandPluginRow(final String pluginKey)
    {
        final WebElement pluginContainer = Iterables.find(driver.findElements(ALL_PLUGINS), new Predicate<WebElement>()
        {
            public boolean apply(WebElement input)
            {
                return input.findElement(HIDDEN_INPUT).getAttribute("value").contains(pluginKey);

            }
        });
        Function<WebDriver, Boolean> isPanelExpanded = new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver from)
            {
                // need to extract id and create new selector because we might not be using  
                // the same driver as outside the closure
                final By pluginDetails = By.cssSelector("#" + pluginContainer.getAttribute("id") 
                                                        + " .upm-details.loaded");
                return Check.elementIsVisible(pluginDetails, driver);
            }
        };
        // don't click it if its already expanded!
        if (!isPanelExpanded.apply(driver))
        {
            pluginContainer.findElement(PLUGIN_ROW).click();
            driver.waitUntil(isPanelExpanded);
        }
        return pluginContainer;
    }

    public boolean pluginHasDisableButton(String pluginKey)
    {
        return getPluginDisableButton(pluginKey) != null;
    }

    public boolean pluginHasUpgradeButton(String pluginKey)
    {
        return getPluginUpgradeButton(pluginKey) != null;
    }
    
    public void clickPluginDisableButton(String pluginKey)
    {
        final WebElement pluginRow = expandPluginRow(pluginKey);
        getPluginDisableButton(pluginKey).click();
        driver.waitUntil(new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver from)
            {
                return pluginRow.getAttribute("class").contains("disabled");
            }
        });
    }

    private WebElement getPluginDisableButton(String pluginKey)
    {
        WebElement pluginRow = expandPluginRow(pluginKey);
        return pluginRow.findElement(DISABLE_BUTTON);
    }

    private WebElement getPluginUpgradeButton(String pluginKey)
    {
        WebElement pluginRow = expandPluginRow(pluginKey);
        try {
            return pluginRow.findElement(UPGRADE_BUTTON);
        } 
        catch (NoSuchElementException e)
        {
            return null;
        }
    }
    
    public enum Version
    {
        VER_4_0("4.0", "/upm/rest/plugins/1.0/product-upgrades/466/compatibility"),
        VER_4_0_1("4.0.1", "/upm/rest/plugins/1.0/product-upgrades/471/compatibility"),
        VER_4_0_2("4.0.2", "/upm/rest/plugins/1.0/product-upgrades/473/compatibility"),
        VER_4_1("4.1", "/upm/rest/plugins/1.0/product-upgrades/520/compatibility");
        
        public final String display;
        public final String value;
        private Version(String display, String value)
        {
            this.display = display;
            this.value = value;
        }
    }
    
    public enum Category implements TabCategory
    {
        INCOMPATIBLE_SECTION("incompatible", INCOMPATIBLE_PLUGINS_SECTION),
        NEED_UPGRADE_SECTION("need-upgrade", NEED_UPGRADE_PLUGINS),
        NEED_PRODUCT_UPGRADE_SECTION("need-product-upgrade", NEED_PRODUCT_UPGRADE_PLUGINS_SECTION),
        COMPATIBLE_SECTION("compatible", COMPATIBLE_PLUGINS_SECTION),
        UNKNOWN_SECTION("unknown", UNKNOWN_PLUGINS_SECTION);

        private final String optionValue;
        private final By containerSelector;

        private Category(String optionValue, By containerSelector)
        {
            this.optionValue = optionValue;
            this.containerSelector = containerSelector;
        }

        public String getOptionValue()
        {
            return optionValue;
        }

        public By getContainerSelector()
        {
            return containerSelector;
        }

    }
}