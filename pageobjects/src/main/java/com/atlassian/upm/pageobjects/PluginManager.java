package com.atlassian.upm.pageobjects;

import java.net.URI;

import javax.inject.Inject;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.AtlassianWebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.atlassian.upm.pageobjects.ChangesRequiringRestart.REQUIRES_RESTART_MESSAGE_ID;
import static com.atlassian.upm.pageobjects.PluginManager.Tab.AUDIT_LOG_TAB;
import static com.atlassian.upm.pageobjects.PluginManager.Tab.COMPATIBILITY_TAB;
import static com.atlassian.upm.pageobjects.PluginManager.Tab.INSTALL_TAB;
import static com.atlassian.upm.pageobjects.PluginManager.Tab.MANAGE_EXISTING_TAB;
import static com.atlassian.upm.pageobjects.PluginManager.Tab.UPGRADE_TAB;
import static com.atlassian.upm.pageobjects.Waits.selected;

public class PluginManager implements Page
{
    private @Inject AtlassianWebDriver driver;
    private @Inject PageBinder binder;

    @ElementBy(id="upm-tab-log")
    private PageElement auditLogTabButton;

    @ElementBy(id="upm-tab-compatibility")
    private PageElement compatibilityTabButton;

    @ElementBy(id="upm-tab-install")
    private PageElement installTabButton;

    @ElementBy(id="upm-tab-manage")
    private PageElement manageExistingTabButton;

    @ElementBy(id="upm-tab-upgrade")
    private PageElement upgradeTabButton;

    @ElementBy(id="upm-pac-unavailable")
    private PageElement pacUnavailableDiv;

    @ElementBy(id="upm-pac-disabled")
    private PageElement pacDisabledDiv;

    @ElementBy(id="upm-pac-checking-availability")
    private PageElement pacCheckingdDiv;

    @ElementBy(id="upm-messages")
    private PageElement messages;

    public String getUrl()
    {
        return "/plugins/servlet/upm";
    }

    /**
     * Shortcut method that opens the install tab, opens the "Upload plugin" dialog and waits for the plugin to be
     * installed.
     *
     * @param uri {@code URI} of plugin to install
     */
    public void uploadPlugin(URI uri)
    {
        openInstallTab().uploadPlugin(uri);
    }

    public void uninstallPlugin(String key)
    {
        openManageExistingTab().uninstallPlugin(key);
    }

    public AuditLogTab openAuditLogTab()
    {
        if (!isTabSelected(AUDIT_LOG_TAB))
        {
            auditLogTabButton.click();
        }
        return binder.bind(AuditLogTab.class);
    }

    public CompatibilityTab openCompatibilityTab()
    {
        if (!isTabSelected(COMPATIBILITY_TAB))
        {
            compatibilityTabButton.click();
        }
        return binder.bind(CompatibilityTab.class);
    }

    public InstallTab openInstallTab()
    {
        if (!isTabSelected(INSTALL_TAB))
        {
            installTabButton.click();
        }
        return binder.bind(InstallTab.class);
    }

    public ManageExistingTab openManageExistingTab()
    {
        if (!isTabSelected(MANAGE_EXISTING_TAB))
        {
            manageExistingTabButton.click();
        }
        return binder.bind(ManageExistingTab.class);
    }

    public UpgradeTab openUpgradeTab()
    {
        if (!isTabSelected(UPGRADE_TAB))
        {
            upgradeTabButton.click();
        }
        return binder.bind(UpgradeTab.class);
    }

    public boolean hasChangesRequiringRestart()
    {
        return driver.elementIsVisible(By.id(REQUIRES_RESTART_MESSAGE_ID));
    }

    public ChangesRequiringRestart getChangesRequiringRestart()
    {
        if (!hasChangesRequiringRestart())
        {
            throw new RuntimeException("There are no changes requiring restart");
        }
        return binder.bind(ChangesRequiringRestart.class);
    }

    public void cancelChangeRequiringRestart(String key)
    {
        getChangesRequiringRestart().cancelChange(key);
    }

    public boolean isTabSelected(Tab tab)
    {
        return selected(getTabPanelElement(tab)).apply(driver);
    }

    public boolean isPacDisabled()
    {
        Poller.waitUntilFalse(pacCheckingdDiv.timed().isPresent());
        return pacDisabledDiv.isVisible();
    }

    public boolean isPacReachable()
    {
        // If the pacAvailabilityDiv is present after the message has changed, that means
        // it is displaying "Could not contact PAC"
        Poller.waitUntilFalse(pacCheckingdDiv.timed().isPresent());
        return !pacUnavailableDiv.isVisible();
    }

    public PageElement getMessages()
    {
        return messages;
    }

    public String getTabLabel(Tab tab)
    {
        return getTabElement(tab).getText();
    }

    private WebElement getTabElement(Tab tab)
    {
        return driver.findElement(By.id("upm-tab-" + tab.getName().toLowerCase()));
    }

    private WebElement getTabPanelElement(Tab tab)
    {
        return driver.findElement(By.id("upm-panel-" + tab.getName().toLowerCase()));
    }

    public enum Tab
    {
        UPGRADE_TAB("upgrade"),
        MANAGE_EXISTING_TAB("manage"),
        INSTALL_TAB("install"),
        COMPATIBILITY_TAB("compatibility"),
        AUDIT_LOG_TAB("log"),
        OSGI_TAB("osgi");

        private final String name;

        Tab(String name)
        {
            this.name = name;
        }

        public String getName()
        {
            return name;
        }
    }

}
