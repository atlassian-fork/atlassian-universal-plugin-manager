package com.atlassian.upm.pageobjects;

import com.google.common.base.Predicate;

import org.openqa.selenium.WebElement;

public final class Predicates
{
    public static Predicate<? super WebElement> whereText(final Predicate<String> p)
    {
        return new Predicate<WebElement>()
        {
            public boolean apply(WebElement e)
            {
                return p.apply(e.getText());
            }
        };
    }
}
