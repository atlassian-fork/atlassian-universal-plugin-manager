package com.atlassian.upm.pageobjects;

import javax.inject.Inject;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Function;
import com.google.common.base.Supplier;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static com.atlassian.upm.pageobjects.Suppliers.findElement;
import static com.atlassian.upm.pageobjects.Waits.and;
import static com.atlassian.upm.pageobjects.Waits.elementIsVisible;
import static com.atlassian.upm.pageobjects.Waits.loaded;
import static com.atlassian.upm.pageobjects.WebElements.PLUGIN_ROW;
import static com.atlassian.upm.pageobjects.WebElements.UPM_DETAILS;

public class InstalledPlugin
{
    private @Inject AtlassianWebDriver driver;
    private @Inject PageBinder binder;

    private final Supplier<WebElement> plugin;
    private final Supplier<WebElement> pluginRow;
    private final Supplier<WebElement> pluginDetails;

    public InstalledPlugin(Supplier<WebElement> plugin)
    {
        this.plugin = plugin;
        pluginRow = findElement(PLUGIN_ROW, plugin);
        pluginDetails = findElement(UPM_DETAILS, plugin);
    }

    public InstalledPluginDetails openPluginDetails()
    {
        if (!driver.elementExistsAt(UPM_DETAILS, plugin.get()) || !driver.elementIsVisibleAt(UPM_DETAILS, plugin.get()))
        {
            pluginRow.get().click();
            driver.waitUntil(and(elementIsVisible(UPM_DETAILS, plugin), loaded(pluginDetails.get())));
        }
        return binder.bind(InstalledPluginDetails.class, pluginDetails, this);
    }

    public InstalledPluginDetails uninstall()
    {
        InstalledPluginDetails pluginDetails = openPluginDetails();
        pluginDetails.uninstall().confirm();
        driver.waitUntil(isDisabledF());
        return pluginDetails;
    }

    public boolean isEnabled()
    {
        return !isDisabled();
    }

    public boolean isDisabled()
    {
        return isDisabledF().apply(driver);
    }

    public Function<WebDriver, Boolean> isDisabledF()
    {
        return Waits.isDisabled(plugin.get());
    }

    public Function<WebDriver, Boolean> isEnabledF()
    {
        return Waits.isEnabled(plugin.get());
    }

    public boolean isExpanded()
    {
        return plugin.get().getAttribute("class").contains("expanded");
    }

    public void enable()
    {
        openPluginDetails().enable();
    }

    public void disable()
    {
        openPluginDetails().disable();
    }

    Supplier<WebElement> getWebElement()
    {
        return plugin;
    }
}
