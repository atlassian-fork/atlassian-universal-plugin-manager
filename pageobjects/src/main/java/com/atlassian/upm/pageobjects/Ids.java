package com.atlassian.upm.pageobjects;

public class Ids
{
    public static long createHash(String key, String tab)
    {
        // Updating this requires updating createHash in upm.js
        long hash = 0;
        long mask = 0xffffffffL;

        String input = tab == null ? key : key + "-" + tab;
        for (int i = 0; i < input.length(); i++)
        {
            hash += input.charAt(i);
            hash += hash << 10;
            hash &= mask;
            hash ^= hash >>> 6;
        }

        hash += hash << 3;
        hash &= mask;
        hash ^= hash >>> 11;
        hash += hash << 15;
        hash &= mask;

        return hash;
    }
}
