package com.atlassian.upm.pageobjects;

import javax.inject.Inject;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Supplier;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public abstract class ManageExistingTabSection
{
    protected @Inject PageBinder binder;
    protected @Inject AtlassianWebDriver driver;

    public ManageExistingTabSection()
    {
        super();
    }

    abstract Supplier<WebElement> find(String key);

    public InstalledPlugin getPlugin(String key)
    {
        return binder.bind(InstalledPlugin.class, find(key));
    }

    public boolean contains(String key)
    {
        try
        {
            find(key).get();
            return true;
        }
        catch (NoSuchElementException e)
        {
            return false;
        }
    }

}
