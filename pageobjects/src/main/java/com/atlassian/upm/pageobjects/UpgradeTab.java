package com.atlassian.upm.pageobjects;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.atlassian.upm.pageobjects.Waits.loading;
import static com.atlassian.upm.pageobjects.Waits.not;
import static com.atlassian.upm.pageobjects.Waits.selectedAndLoaded;
import static com.atlassian.upm.pageobjects.WebElements.PLUGIN;
import static com.atlassian.upm.pageobjects.WebElements.findPluginElement;
import static com.google.common.base.Suppliers.ofInstance;

public class UpgradeTab extends UPMTab
{
    private @Inject PageBinder binder;

    @FindBy(id="upm-panel-upgrade")
    private WebElement upgradePanel;

    @FindBy(id="upm-upgrade-all")
    private WebElement upgradeAllButton;

    @FindBy(id="upm-messages")
    private WebElement messages;

    @FindBy(id="upm-available-upgrades")
    private WebElement availableUpgrades;

    @WaitUntil
    public void waitUntilTabIsLoaded()
    {
        driver.waitUntil(selectedAndLoaded(upgradePanel));
    }

    @WaitUntil
    public void waitUntilAvailableUpgradesIsLoaded()
    {
        driver.waitUntil(not(loading(availableUpgrades)));
    }

    public boolean contains(String key)
    {
        try
        {
            getPlugin(key).getWebElement().get();
            return true;
        }
        catch (NoSuchElementException e)
        {
            return false;
        }
    }

    public UpgradeablePlugin getPlugin(String key)
    {
        return binder.bind(UpgradeablePlugin.class, findPluginElement(key, "upgrade", ofInstance(upgradePanel)));
    }

    public UpgradeTab upgradeAll()
    {
        upgradeAllButton.click();
        driver.waitUntilElementIsVisibleAt(By.className("upgrade"), messages);
        return this;
    }

    public WebElement getUpgradeAllButton()
    {
        return upgradeAllButton;
    }

    public Iterable<UpgradeablePlugin> getPlugins()
    {
        List<UpgradeablePlugin> plugins = new LinkedList<UpgradeablePlugin>();
        for (WebElement plugin : upgradePanel.findElements(PLUGIN))
        {
            plugins.add(binder.bind(UpgradeablePlugin.class, ofInstance(plugin)));
        }
        return plugins;
    }

    @Override
    protected String getPluginContainerId(TabCategory category)
    {
        return "#upm-upgrade-plugins";
    }

    public enum Category implements TabCategory
    {
        DEFAULT;
        public String getOptionValue()
        {
            return "";
        }
    }
}