package com.atlassian.upm.pageobjects;

import javax.inject.Inject;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Predicate;
import com.google.common.base.Supplier;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.atlassian.upm.pageobjects.Ids.createHash;
import static com.atlassian.upm.pageobjects.Suppliers.findElement;

import static com.google.common.collect.Iterables.*;

public class PluginModuleManager
{
    private static final By MODULES_CONTAINTER = By.className("upm-module-container");

    private @Inject AtlassianWebDriver driver;
    private @Inject PageBinder binder;
    
    private final Supplier<WebElement> modules;

    public PluginModuleManager(Supplier<WebElement> modules)
    {
        this.modules = modules;
    }
    
    @WaitUntil
    public void waitUntilModulesContainerIsVisible()
    {
        driver.waitUntilElementIsVisibleAt(MODULES_CONTAINTER, modules.get());
    }

    public PluginModule getModule(String key)
    {
        return binder.bind(PluginModule.class, findModuleElement(key, modules));
    }

    static Supplier<WebElement> findModuleElement(final String key, final Supplier<WebElement> e)
    {
        return findElement(By.id("upm-plugin-module-" + createHash(key, null)), e);
    }
    
    public boolean allPluginModuleActionsAreHidden()
    {
        return all(driver.findElements(By.className("upm-module-actions")), hasClass("hidden"));
    }

    private Predicate<? super WebElement> hasClass(final String className)
    {
        return new Predicate<WebElement>()
        {
            public boolean apply(WebElement e)
            {
                return e.getAttribute("class").contains(className);
            }
        };
    }
}
