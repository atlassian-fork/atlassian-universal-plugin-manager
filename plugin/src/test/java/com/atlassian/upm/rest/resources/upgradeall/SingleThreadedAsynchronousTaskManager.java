package com.atlassian.upm.rest.resources.upgradeall;

import java.net.URI;
import java.util.concurrent.Executors;

import javax.ws.rs.core.Response;

import com.atlassian.upm.AsyncTaskAwareApplicationProperties;
import com.atlassian.upm.AsyncTaskAwareUserManager;
import com.atlassian.upm.rest.UpmUriBuilder;
import com.atlassian.upm.rest.async.AsynchronousTask;
import com.atlassian.upm.rest.async.AsynchronousTaskManager;
import com.atlassian.upm.rest.async.TaskStatus;

public class SingleThreadedAsynchronousTaskManager extends AsynchronousTaskManager
{
    private AsynchronousTask<? extends TaskStatus> currentlyRunning;
    private boolean wasTaskSubmitted = false;

    public SingleThreadedAsynchronousTaskManager(UpmUriBuilder uriBuilder, AsyncTaskAwareUserManager asyncTaskAwareUserManager,
            AsyncTaskAwareApplicationProperties applicationProperties)
    {
        super(Executors.newSingleThreadExecutor(), uriBuilder, asyncTaskAwareUserManager, applicationProperties);
    }

    @Override
    public <T extends TaskStatus> Response executeAsynchronousTask(final AsynchronousTask<T> task)
    {
        wasTaskSubmitted = true;
        task.accept();
        AsynchronousTask<T> realTask = new AsynchronousTask<T>(task.getType(), task.getUsername())
        {
            public URI call() throws Exception
            {
                currentlyRunning = task;
                return task.call();
            }

            public void accept()
            {
                task.accept();
            }

            public Representation<T> getRepresentation(UpmUriBuilder uriBuilder)
            {
                return task.getRepresentation(uriBuilder);
            }
        };
        return super.executeAsynchronousTask(realTask);
    }

    public AsynchronousTask<? extends TaskStatus> getCurrentlyRunningTask()
    {
        return currentlyRunning;
    }

    public boolean wasTaskSubmitted()
    {
        return wasTaskSubmitted;
    }
}
