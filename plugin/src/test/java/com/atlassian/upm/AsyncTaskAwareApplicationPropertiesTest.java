package com.atlassian.upm;

import com.atlassian.sal.api.ApplicationProperties;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AsyncTaskAwareApplicationPropertiesTest
{
    private final static String BASE_URL = "http://baseurl:1234";
    
    AsyncTaskAwareApplicationProperties applicationProperties;
    @Mock ApplicationProperties baseApplicationProperties;

    @Before
    public void createUriBuilder()
    {
        when(baseApplicationProperties.getBaseUrl()).thenReturn(BASE_URL);
        applicationProperties = new AsyncTaskAwareApplicationProperties(baseApplicationProperties);
    }
    
    @Test
    public void assertThatApplicationPropertiesBaseUrlIsUsedByDefault()
    {
        assertThat(applicationProperties.getBaseUrl(), is(equalTo(BASE_URL)));
    }
    
    @Test
    public void assertThatSpecifiedBaseUrlIsUsedWhenSpecified()
    {
        final String newBaseUrl = "http://newbaseurl:5678";
        applicationProperties.setBaseUrl(newBaseUrl);

        assertThat(applicationProperties.getBaseUrl(), is(equalTo(newBaseUrl)));
    }
}
