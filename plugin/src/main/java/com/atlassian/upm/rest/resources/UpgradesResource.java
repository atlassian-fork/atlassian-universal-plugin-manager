package com.atlassian.upm.rest.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.atlassian.plugins.domain.model.plugin.PluginVersion;
import com.atlassian.upm.pac.PacClient;
import com.atlassian.upm.rest.representations.RepresentationFactory;
import com.atlassian.upm.rest.resources.permission.PermissionEnforcer;

import static com.atlassian.upm.rest.MediaTypes.UPGRADES_JSON;
import static com.atlassian.upm.spi.Permission.GET_AVAILABLE_PLUGINS;
import static com.google.common.base.Preconditions.checkNotNull;

@Path("/upgrades")
public class UpgradesResource
{
    private final PacClient client;
    private final RepresentationFactory factory;
    private final PermissionEnforcer permissionEnforcer;

    public UpgradesResource(PacClient client, RepresentationFactory factory, PermissionEnforcer permissionEnforcer)
    {
        this.permissionEnforcer = checkNotNull(permissionEnforcer, "permissionEnforcer");
        this.client = checkNotNull(client, "client");
        this.factory = checkNotNull(factory, "factory");
    }

    @GET
    @Produces(UPGRADES_JSON)
    public Response get()
    {
        permissionEnforcer.enforcePermission(GET_AVAILABLE_PLUGINS);
        Iterable<PluginVersion> upgrades = client.getUpgrades();
        return Response.ok(factory.createUpgradesRepresentation(upgrades)).build();
    }
}
