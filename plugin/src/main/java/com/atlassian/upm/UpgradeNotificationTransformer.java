package com.atlassian.upm;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.transformer.AbstractStringTransformedDownloadableResource;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformer;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.upm.impl.NamespacedPluginSettings;
import com.atlassian.upm.rest.UpmUriBuilder;

import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Qualifier;

import static com.atlassian.upm.PluginUpgradeCheckScheduler.KEY;
import static com.atlassian.upm.PluginUpgradeCheckScheduler.KEY_PREFIX;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

public class UpgradeNotificationTransformer implements WebResourceTransformer
{
    private final ApplicationProperties applicationProperties;
    private final UpmUriBuilder uriBuilder;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final I18nResolver i18nResolver;

    public UpgradeNotificationTransformer(
        @Qualifier("applicationProperties") ApplicationProperties applicationProperties,
        UpmUriBuilder uriBuilder,
        PluginSettingsFactory pluginSettingsFactory,
        I18nResolver i18nResolver)
    {
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.uriBuilder = checkNotNull(uriBuilder, "uriBuilder");
        this.pluginSettingsFactory = checkNotNull(pluginSettingsFactory, "pluginSettingsFactory");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
    }

    public DownloadableResource transform(Element configElement, ResourceLocation location, String filePath, DownloadableResource nextResource)
    {
        return new AbstractStringTransformedDownloadableResource(nextResource)
        {
            @Override
            protected String transform(String originalContent)
            {
                return originalContent
                    .replaceFirst("productId", format("productId = '%s'",
                        applicationProperties.getDisplayName().toLowerCase()))
                    .replaceFirst("productVersion", format("productVersion = '%s'",
                        applicationProperties.getVersion()))
                    .replaceFirst("upmUri", format("upmUri = '%s'",
                        uriBuilder.buildUpmUri()))
                    .replaceFirst("upgradeCount", format("upgradeCount = %s",
                        new NamespacedPluginSettings(pluginSettingsFactory.createGlobalSettings(), KEY_PREFIX).get(KEY)))
                    .replaceFirst("upm.upgrade.notification.title",
                        i18nResolver.getText("upm.upgrade.notification.title"))
                    .replaceFirst("upm.upgrade.notification.body",
                        i18nResolver.getText("upm.upgrade.notification.body"))
                    .replaceFirst("upm.upgrade.notification.body.singular",
                        i18nResolver.getText("upm.upgrade.notification.body.singular"))
                    .replaceFirst("upm.upgrade.notification.body.dismiss",
                        i18nResolver.getText("upm.upgrade.notification.body.dismiss"));
            }
        };
    }
}
