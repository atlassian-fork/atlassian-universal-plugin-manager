package com.atlassian.upm.test;

public final class AsynchronousTaskException extends RuntimeException
{
    AsynchronousTaskException(String message)
    {
        super(message);
    }
}
