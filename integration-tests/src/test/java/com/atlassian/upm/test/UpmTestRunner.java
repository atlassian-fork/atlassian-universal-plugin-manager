package com.atlassian.upm.test;

import java.io.File;

import com.atlassian.integrationtesting.ApplicationPropertiesImpl;
import com.atlassian.integrationtesting.runner.CompositeTestRunner;
import com.atlassian.integrationtesting.runner.TestGroupRunner;
import com.atlassian.integrationtesting.ui.Bamboo;
import com.atlassian.integrationtesting.ui.Confluence;
import com.atlassian.integrationtesting.ui.FeCru;
import com.atlassian.integrationtesting.ui.Jira;
import com.atlassian.integrationtesting.ui.RefApp;
import com.atlassian.integrationtesting.ui.RunningTestGroup;
import com.atlassian.integrationtesting.ui.UiRunListener;
import com.atlassian.integrationtesting.ui.UiTestRunner;
import com.atlassian.integrationtesting.ui.UiTester;
import com.atlassian.integrationtesting.ui.UiTesterFunctionProvider;
import com.atlassian.integrationtesting.ui.UiTesters;
import com.atlassian.sal.api.ApplicationProperties;

import com.google.common.collect.ImmutableList;
import com.google.inject.AbstractModule;
import com.google.inject.ConfigurationException;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.spi.Message;

import org.junit.runners.model.InitializationError;

import static com.atlassian.integrationtesting.ApplicationPropertiesImpl.getStandardApplicationProperties;
import static org.apache.commons.lang.StringUtils.isBlank;

public final class UpmTestRunner extends CompositeTestRunner
{
    public UpmTestRunner(Class<?> klass) throws InitializationError
    {
        super(klass, compose());
    }

    public static Composer compose()
    {
        Injector injector = Guice.createInjector(new UpmModule());
        return CompositeTestRunner.compose().
            from(UiTestRunner.compose(injector));
    }

    static final class UpmModule extends AbstractModule
    {
        @Override
        protected void configure() {}

        @Provides
        @Singleton
        @RunningTestGroup
        String getRunningTestGroup()
        {
            if (isBlank(TestGroupRunner.getRunningTestGroup()))
            {
                throw new ConfigurationException(ImmutableList.of(new Message("No testGroup configured - can't figure out which UiTester to use without a test group in the form {application}-v{version}")));
            }
            return TestGroupRunner.getRunningTestGroup();
        }

        @Provides @Singleton ApplicationProperties getApplicationProperties()
        {
            return ApplicationPropertiesImpl.getStandardApplicationProperties();
        }

        @Provides @Singleton @UiRunListener.OutputDirectory
        File outputDirectory(@RunningTestGroup String runningTestGroup)
        {
            return new File("target", runningTestGroup + "-htmlunit");
        }

        
        @Provides @Singleton RestTester getRestTester()
        {
            return new RestTester();
        }

        @Provides @Singleton UiTester getUiTester(ApplicationProperties applicationProperties,
                @RunningTestGroup String runningTestGroup)
        {
            return Testers.valueOf(runningTestGroup.toUpperCase()).get(applicationProperties);
        }

        @Provides @Singleton UpmUiTester getUpmUiTester(UiTester uiTester)
        {
            return new UpmUiTester(uiTester);
        }

        public UiTester newUiTester()
        {
            return Testers.valueOf(TestGroupRunner.getRunningTestGroup().toUpperCase()).get(getStandardApplicationProperties());
        }

        enum Testers
        {
            REFAPP(RefApp.v2_7),
            JIRA(Jira.v4_3),
            CONFLUENCE(Confluence.v3_4),
            BAMBOO(Bamboo.v3_0),
            FECRU(FeCru.v2_3);

            private final UiTesterFunctionProvider provider;

            private Testers(UiTesterFunctionProvider provider)
            {
                this.provider = provider;
            }

            public final UpmUiTester get(ApplicationProperties applicationProperties)
            {
                return new UpmUiTester(UiTesters.newUiTester(applicationProperties, provider));
            }
        }
    }
}