package it.com.atlassian.upm.rest.resources;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.upm.rest.representations.AbstractInstallablePluginCollectionRepresentation;
import com.atlassian.upm.rest.representations.AvailablePluginCollectionRepresentation;
import com.atlassian.upm.test.RestTester;
import com.atlassian.upm.test.TestPlugins;
import com.atlassian.upm.test.UpmResourceTestBase;

import com.google.common.base.Function;
import com.sun.jersey.api.client.ClientResponse;

import org.junit.Test;

import static com.atlassian.upm.test.JerseyClientMatchers.ok;
import static com.atlassian.upm.test.JerseyClientMatchers.unauthorized;
import static com.atlassian.upm.test.UpmIntegrationMatchers.containsPlugins;
import static com.atlassian.upm.test.UpmTestGroups.REFAPP;
import static com.google.common.collect.Iterables.transform;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AvailablePluginCollectionResourceIntegrationTest extends UpmResourceTestBase
{
    @Test
    public void assertThatAvailablePluginsResourceContainsAllPlugins()
    {
        AvailablePluginCollectionRepresentation availablePlugins = restTester.getAvailablePlugins();
        assertThat(availablePlugins.getPlugins(), containsPlugins(
            TestPlugins.OBR,
            TestPlugins.NONDEPLOYABLE,
            TestPlugins.INSTALLABLE,
            TestPlugins.NOT_ENABLED_BY_DEFAULT,
            TestPlugins.USER_INSTALLED_WITH_MODULES
        ));
    }

    @Test
    public void assertThatAvailablePluginsResourceContainsSameAmountAsMaxResults()
    {
        AvailablePluginCollectionRepresentation availablePlugins = restTester.getAvailablePlugins(1, 0);
        assertThat(availablePlugins.getPlugins(), containsPlugins(
            TestPlugins.OBR
        ));
    }

    @Test
    public void assertThatAvailablePluginsResourceStartsWithSpecifiedStartIndex()
    {
        AvailablePluginCollectionRepresentation availablePlugins = restTester.getAvailablePlugins(0, 1);
        assertThat(availablePlugins.getPlugins(), containsPlugins(
            TestPlugins.NONDEPLOYABLE,
            TestPlugins.INSTALLABLE,
            TestPlugins.NOT_ENABLED_BY_DEFAULT,
            TestPlugins.USER_INSTALLED_WITH_MODULES
        ));
    }

    @Test
    public void assertThatAvailablePluginsHasSafeModeFalseWhenNotInSafeMode()
    {
        AvailablePluginCollectionRepresentation availablePlugins = restTester.getAvailablePlugins();
        assertFalse(availablePlugins.isSafeMode());
    }

    @Test
    public void assertThatAvailablePluginsHasSafeModeTrueWhenInSafeMode()
    {
        restTester.enterSafeMode();
        try
        {
            AvailablePluginCollectionRepresentation availablePlugins = restTester.getAvailablePlugins();
            assertTrue(availablePlugins.isSafeMode());
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }

    @Test
    public void assertThatSearchReturnsPluginsWithNamesThatMatchQuery()
    {
        AvailablePluginCollectionRepresentation plugins = restTester.searchPlugins("test");
        assertThat(transform(plugins.getPlugins(), toPluginNames()), everyItem(anyOf(containsString("test"), containsString("Test"))));
    }

    @Test
    public void assertThatPluginSearchResourceContainsSameAmountAsMaxResults()
    {
        AvailablePluginCollectionRepresentation plugins = restTester.searchPlugins("test", 1, 0);
        assertThat(plugins.getPlugins(), containsPlugins(
            TestPlugins.OBR
        ));
    }

    @Test
    public void assertThatPluginSearchResourceStartsWithSpecifiedStartIndex()
    {
        AvailablePluginCollectionRepresentation plugins = restTester.searchPlugins("test", 0, 1);
        assertThat(plugins.getPlugins(), containsPlugins(
            TestPlugins.NONDEPLOYABLE,
            TestPlugins.INSTALLABLE,
            TestPlugins.NOT_ENABLED_BY_DEFAULT,
            TestPlugins.USER_INSTALLED_WITH_MODULES
        ));
    }

    /**
     * Refapp-specific tests for the AvailablePluginCollectionResource because other apps don't have users other than admin.
     * For example, barney and fred only exist in refapp, so this can be tested only there.
     */
    @Test
    @TestGroups(REFAPP)
    public void unauthorizedResponseForNonAdmins()
    {
        RestTester restTesterBarney = new RestTester("barney", "barney");
        try
        {
            final ClientResponse responseNonAdmin = restTesterBarney.getAvailablePlugins(ClientResponse.class);
            assertThat(responseNonAdmin, is(unauthorized()));
        }
        finally
        {
            restTesterBarney.destroy();
        }
    }

    @Test
    public void okResponseForAdmins()
    {
        final ClientResponse responseAdmin = restTester.getAvailablePlugins(ClientResponse.class);
        assertThat(responseAdmin, is(ok()));
    }

    private static Function<AbstractInstallablePluginCollectionRepresentation.AvailablePluginEntry, String> toPluginNames()
    {
        return new Function<AbstractInstallablePluginCollectionRepresentation.AvailablePluginEntry, String>()
        {
            public String apply(AbstractInstallablePluginCollectionRepresentation.AvailablePluginEntry plugin)
            {
                return plugin.getName();
            }
        };
    }
}
