package it.com.atlassian.upm.rest.resources;

import com.atlassian.upm.rest.representations.AvailablePluginRepresentation;
import com.atlassian.upm.test.TestPlugins;
import com.atlassian.upm.test.UpmResourceTestBase;

import com.sun.jersey.api.client.ClientResponse;

import org.junit.Ignore;
import org.junit.Test;

import static com.atlassian.upm.test.JerseyClientMatchers.badProxy;
import static com.atlassian.upm.test.JerseyClientMatchers.notFound;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AvailablePluginResourceIntegrationTest extends UpmResourceTestBase
{
    @Test
    public void assertThatAvailablePluginResourceCanFetchPluginDetails()
    {
        AvailablePluginRepresentation availablePlugin = restTester.getAvailablePlugin(TestPlugins.INSTALLABLE);

        String key = AvailablePlugins.parseKeyFromPath(availablePlugin.getSelfLink().getPath());
        assertThat(key, is(equalTo(TestPlugins.INSTALLABLE.getEscapedKey())));
    }

    @Test
    public void assertThatAvailableNonDeployablePluginResourceHasDeployableFlagToFalse()
    {
        AvailablePluginRepresentation availablePlugin = restTester.getAvailablePlugin(TestPlugins.NONDEPLOYABLE);

        assertFalse(availablePlugin.isDeployable());
    }

    @Test
    public void assertThatAvailableDeployablePluginResourceHasDeployableFlagToTrue()
    {
        AvailablePluginRepresentation availablePlugin = restTester.getAvailablePlugin(TestPlugins.INSTALLABLE);

        assertTrue(availablePlugin.isDeployable());
    }

    @Test
    public void assertThatPluginThatDoesNotExistOnPacCausesNotFoundResponse()
    {
        ClientResponse response = restTester.getAvailablePlugin("non-existent-plugin", ClientResponse.class);
        assertThat(response, is(notFound()));
    }

    @Test
    public void assertThatWhenPacRespondsWithInternalErrorBadProxyResponseIsReturned()
    {
        ClientResponse response = restTester.getAvailablePlugin("trigger-500", ClientResponse.class);
        assertThat(response, is(badProxy()));
    }
}
