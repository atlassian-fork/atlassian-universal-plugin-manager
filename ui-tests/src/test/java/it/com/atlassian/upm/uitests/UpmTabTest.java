package it.com.atlassian.upm.uitests;

import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.page.HomePage;
import com.atlassian.pageobjects.page.LoginPage;
import com.atlassian.pageobjects.page.WebSudoPage;
import com.atlassian.upm.pageobjects.PluginManager;
import com.atlassian.upm.test.UpmUiTestRunner;
import com.atlassian.webdriver.pageobjects.WebDriverTester;

import com.google.inject.Inject;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;

@RunWith(UpmUiTestRunner.class)
public class UpmTabTest
{
    private static @Inject TestedProduct<WebDriverTester> product;
    private static PluginManager upm;

    @BeforeClass
    public static void setUp()
    {
        upm = product.visit(LoginPage.class).
            loginAsSysAdmin(WebSudoPage.class).
            confirm(PluginManager.class);
    }

    @AfterClass
    public static void tearDown()
    {
        product.visit(HomePage.class).getHeader().logout(LoginPage.class);
    }

    @Before
    public void loadTab()
    {
        reloadPage();
    }

    @Test
    public void testUpgradeTab()
    {
        upm.openUpgradeTab();
        assertTrue(upm.isTabSelected(PluginManager.Tab.UPGRADE_TAB));
    }

    @Test
    public void testUpgradeTabLabelWithNumberOfUpgradesWhenLoadingAnotherTab()
    {
        upm.openManageExistingTab();
        assertThat(upm.getTabLabel(PluginManager.Tab.UPGRADE_TAB), is(equalTo("Upgrade (6)")));
    }

    @Test
    public void testInstallTab()
    {
        upm.openInstallTab();
        assertTrue(upm.isTabSelected(PluginManager.Tab.INSTALL_TAB));
    }

    @Test
    public void testManageExistingTab()
    {
        upm.openManageExistingTab();
        assertTrue(upm.isTabSelected(PluginManager.Tab.MANAGE_EXISTING_TAB));
    }

    @Test
    public void testCompatibilityTab()
    {
        upm.openCompatibilityTab();
        assertTrue(upm.isTabSelected(PluginManager.Tab.COMPATIBILITY_TAB));
    }

    @Test
    public void testAuditLogTab()
    {
        upm.openAuditLogTab();
        assertTrue(upm.isTabSelected(PluginManager.Tab.AUDIT_LOG_TAB));
    }

    @Test
    public void testLastDisplayedTabIsRememberedOnNextReload()
    {
        upm.openInstallTab();
        reloadPage();

        assertTrue(upm.isTabSelected(PluginManager.Tab.INSTALL_TAB));
    }

    private static void reloadPage()
    {
        upm = product.visit(PluginManager.class);
    }

}
