package com.atlassian.upm.selfupgrade;

import java.io.File;
import java.net.URI;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import static com.atlassian.upm.selfupgrade.Constants.SELFUPGRADE_SETTINGS_JAR_PATH;
import static com.atlassian.upm.selfupgrade.Constants.SELFUPGRADE_SETTINGS_SELFUPGRADE_PLUGIN_URI;
import static com.atlassian.upm.selfupgrade.Constants.SELFUPGRADE_SETTINGS_UPM_KEY;
import static com.atlassian.upm.selfupgrade.Constants.SELFUPGRADE_SETTINGS_UPM_URI;
import static com.google.common.base.Preconditions.checkNotNull;

public class UpgradeParametersFactory
{
    private PluginSettingsFactory pluginSettingsFactory;
    
    public UpgradeParametersFactory(PluginSettingsFactory pluginSettingsFactory)
    {
        this.pluginSettingsFactory = checkNotNull(pluginSettingsFactory, "pluginSettingsFactory");
    }

    /**
     * Returns the previously configured upgrade parameters, or null if they have not been configured.
     */
    public UpgradeParameters getUpgradeParameters()
    {
        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
        
        Object jarPathValue = settings.get(SELFUPGRADE_SETTINGS_JAR_PATH);
        Object upmKeyValue = settings.get(SELFUPGRADE_SETTINGS_UPM_KEY);
        Object upmUriValue = settings.get(SELFUPGRADE_SETTINGS_UPM_URI);
        Object selfUpgradePluginUriValue = settings.get(SELFUPGRADE_SETTINGS_SELFUPGRADE_PLUGIN_URI);
        if (jarPathValue == null || upmKeyValue == null || upmUriValue == null || selfUpgradePluginUriValue == null)
        {
            return null;
        }
        return new UpgradeParameters(new File(jarPathValue.toString()),
                                     upmKeyValue.toString(),
                                     URI.create(upmUriValue.toString()),
                                     URI.create(selfUpgradePluginUriValue.toString()));
    }
    
    /**
     * Resets the component to an unconfigured state.  This should be done after the
     * installation is complete, to ensure that the self-upgrade plugin can only be
     * used once.
     */
    public void reset()
    {
        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
        settings.remove(SELFUPGRADE_SETTINGS_JAR_PATH);
        settings.remove(SELFUPGRADE_SETTINGS_UPM_KEY);
        settings.remove(SELFUPGRADE_SETTINGS_UPM_URI);
        settings.remove(SELFUPGRADE_SETTINGS_SELFUPGRADE_PLUGIN_URI);
    }
}
